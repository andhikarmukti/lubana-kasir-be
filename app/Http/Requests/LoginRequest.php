<?php

namespace App\Http\Requests;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function authenticate()
    {
        $this->configureRateLimiting();

        $credentials = $this->only('username', 'password');

        if (! Auth::attempt($credentials)) {

        RateLimiter::hit($this->input('username'), 300);

        throw ValidationException::withMessages(['error' => 'Username atau Password tidak ditemukan. Silahkan coba kembali!']);

        }
    }

    public function configureRateLimiting()
    {
        $input =  $this->input('username');

        if (! RateLimiter::tooManyAttempts($input, 3)) {
            return;
        }

        event(new Lockout($this));

        $seconds = RateLimiter::availableIn($input);

        throw ValidationException::withMessages([
            'error' => 'Upaya masuk dibatasi. Coba kembali setelah ',
            'time' => $seconds
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
