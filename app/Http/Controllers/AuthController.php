<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request, LoginRequest $loginRequest)
    {
        // return request('g-recaptcha-response');
        $rules = [
            'username' => 'required|alpha_num|max:255|min:5',
            'password' => 'required|string|min:6',
            // 'g-recaptcha-response' => 'required|captcha',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json([
                'success' => false,
                'data' => $validator->errors()->first()
            ], 400);
        }

        return $loginRequest->authenticate();

        $user = User::whereUsername($request->get('username'))->first();
        $token = $user->createToken($user->id)->plainTextToken;

        if (!empty($user) && $user->banned == 0 && Hash::check($request->get('password'), $user->password)) {
            return [
                'success' => true,
                'token' => $token,
                'user' => $user,
            ];
        } else {
            return [
                'success' => false,
                'token' => null,
                'user' => null,
            ];
        }
    }
}
